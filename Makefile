package:
	./gradlew clean build

test:
	./gradlew test

run:
	make package && java -jar build/libs/Power\ Plant\ System-1.0-SNAPSHOT.jar
