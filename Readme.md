## Power Plant System

This is a virtual power plant system for aggregating distributed power sources into
a single cloud based energy provider.

### Following versions of tools are used in the project

    Spring Boot 2.7.5
    Spring Dependency Management Plugin 1.0.15.RELEASE
    Lombok 1.18.24
    JUnit 5.9.0
    Mockito 4.8.1

### Databases

H2 database is used in the project. It is an in memory SQL database. Spring boot 2.7.5 uses H2 of version 2.1.214.

### Build Project

This project uses Gradle as build system. Run the following command from project root directory to build the project

    make package

### Test Project

To run tests, run the following command:

    make test

### Run Project
Run the following command to run the project:

    make run

### Following Rest APIs are available in the project:

    GET /batteries/{from}/{to}  -> from and to are the ranges of postcode

    POST /batteries/   -> Body required for this endpoint is [{ name: "", postcode: "", capacity: "" }]