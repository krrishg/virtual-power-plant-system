package com.krrishg.repository;

import com.krrishg.repository.entities.Battery;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BatteryRepository extends JpaRepository<Battery, Long> {

    List<Battery> findByPostcodeBetweenOrderByNameAsc(String from, String to);
}
