package com.krrishg.rest.controllers;

import com.krrishg.adapters.BatteryAdapter;
import com.krrishg.rest.models.BatteryRequest;
import com.krrishg.usecases.CreateBatteriesUseCase;
import com.krrishg.usecases.FindBatteriesUseCase;
import com.krrishg.usecases.models.BatteryInput;
import com.krrishg.usecases.models.BatteryOutput;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/batteries")
@Validated
public class PowerPlantController {
    private final CreateBatteriesUseCase createBatteriesUseCase;
    private final FindBatteriesUseCase findBatteriesUseCase;

    @PostMapping
    public ResponseEntity<Void> saveBatteries(@Valid @RequestBody List<BatteryRequest> batteries) {
        BatteryInput batteryInput = BatteryAdapter.toBatteryInput(batteries);
        createBatteriesUseCase.execute(batteryInput);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/range/{from}/{to}")
    public ResponseEntity<BatteryOutput> getBatteriesWithinRange(@PathVariable("from") @NotEmpty String from,
                                                                 @PathVariable("to") @NotEmpty String to) {
        BatteryOutput batteryOutput = findBatteriesUseCase.execute(from, to);
        return ResponseEntity.ok(batteryOutput);
    }
}
