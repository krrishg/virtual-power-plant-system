package com.krrishg.rest.models;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BatteryRequest {
    @NotEmpty(message = "Please provide name")
    private String name;
    @NotEmpty(message = "Please provide post code")
    private String postcode;
    @NotNull(message = "Please provide capacity in watt")
    private Integer capacity;
}
