package com.krrishg.rest;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleErrors(
            RuntimeException ex, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage();
        String[] errors = ex.getMessage().split(",");
        Arrays.stream(errors)
                .forEach(error -> errorMessage.getErrors().add(error.trim()));

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }
}

@Getter
@Setter
class ErrorMessage implements Serializable {
    private List<String> errors = new ArrayList<>();
}
