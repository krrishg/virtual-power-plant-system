package com.krrishg.usecases;

import com.krrishg.repository.BatteryRepository;
import com.krrishg.usecases.models.BatteryInput;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateBatteriesUseCase {
    private final BatteryRepository batteryRepository;

    public void execute(BatteryInput batteryInput) {
        batteryRepository.saveAll(batteryInput.getBatteries());
    }

}

