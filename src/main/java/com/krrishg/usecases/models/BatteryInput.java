package com.krrishg.usecases.models;

import com.krrishg.repository.entities.Battery;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BatteryInput {
    private List<Battery> batteries;
}
