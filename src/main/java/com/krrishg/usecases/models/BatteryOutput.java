package com.krrishg.usecases.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BatteryOutput {
    private List<String> names;
    private Long totalCapacity;
    private Double averageCapacity;
}
