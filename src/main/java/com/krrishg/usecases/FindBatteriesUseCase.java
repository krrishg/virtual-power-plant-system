package com.krrishg.usecases;

import com.krrishg.adapters.BatteryAdapter;
import com.krrishg.repository.BatteryRepository;
import com.krrishg.repository.entities.Battery;
import com.krrishg.usecases.models.BatteryOutput;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class FindBatteriesUseCase {
    private final BatteryRepository batteryRepository;

    public BatteryOutput execute(String from, String to) {
        List<Battery> batteries = batteryRepository.findByPostcodeBetweenOrderByNameAsc(from, to);

        if (batteries == null)
            return null;

        IntSummaryStatistics batterySummaryStatistics = batteries.stream()
                .map(Battery::getCapacity)
                .collect(Collectors.summarizingInt(Integer::intValue));

        List<String> names = batteries.stream()
                .map(Battery::getName)
                .collect(Collectors.toList());

        return BatteryAdapter.toBatteryOutput(names, batterySummaryStatistics);
    }

}

