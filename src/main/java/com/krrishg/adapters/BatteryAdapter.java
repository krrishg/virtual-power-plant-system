package com.krrishg.adapters;


import com.krrishg.repository.entities.Battery;
import com.krrishg.rest.models.BatteryRequest;
import com.krrishg.usecases.models.BatteryInput;
import com.krrishg.usecases.models.BatteryOutput;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class BatteryAdapter {

    public static BatteryInput toBatteryInput(List<BatteryRequest> batteryRequest) {
        BatteryInput batteryInput = new BatteryInput();
        batteryInput.setBatteries(new ArrayList<>());

        if (batteryRequest != null) {
            List<Battery> batteries = batteryRequest.stream().map(request -> {
                Battery battery = new Battery();
                battery.setName(request.getName());
                battery.setPostcode(request.getPostcode());
                battery.setCapacity(request.getCapacity());
                return battery;
            }).collect(Collectors.toList());

            batteryInput.setBatteries(batteries);
        }

        return batteryInput;
    }

    public static BatteryOutput toBatteryOutput(List<String> names, IntSummaryStatistics batterySummaryStatistics) {
        BatteryOutput batteryOutput = new BatteryOutput();
        batteryOutput.setNames(names);
        batteryOutput.setAverageCapacity(batterySummaryStatistics.getAverage());
        batteryOutput.setTotalCapacity(batterySummaryStatistics.getSum());
        return batteryOutput;
    }
}
