package com.krrishg.rest.adapters;

import com.krrishg.adapters.BatteryAdapter;
import com.krrishg.repository.entities.Battery;
import com.krrishg.rest.models.BatteryRequest;
import com.krrishg.usecases.models.BatteryInput;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BatteryAdapterTest {
    @Test
    void givenBatteryRequest_whenConvertToBatteryInput_thenShouldBeConvertedToBatteryInput() {
        BatteryRequest firstRequest = new BatteryRequest();
        firstRequest.setCapacity(1);
        firstRequest.setName("first");
        firstRequest.setPostcode("123");

        BatteryRequest secondRequest = new BatteryRequest();
        secondRequest.setCapacity(2);
        secondRequest.setName("second");
        secondRequest.setPostcode("456");

        BatteryInput batteryInput = BatteryAdapter.toBatteryInput(Arrays.asList(firstRequest, secondRequest));
        List<Battery> batteries = batteryInput.getBatteries();

        Battery firstBattery = batteries.get(0);
        Battery secondBattery = batteries.get(1);

        assertEquals(1, firstBattery.getCapacity());
        assertEquals("first", firstBattery.getName());
        assertEquals("123", firstBattery.getPostcode());
        assertEquals(2, secondBattery.getCapacity());
        assertEquals("second", secondBattery.getName());
        assertEquals("456", secondBattery.getPostcode());
    }

    @Test
    void givenNullBatteryRequest_whenConvertToBatteryInput_thenBatteriesShouldNotBeAdded() {
        BatteryInput batteryInput = BatteryAdapter.toBatteryInput(null);
        assertEquals(0, batteryInput.getBatteries().size());
    }
}