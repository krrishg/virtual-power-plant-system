package com.krrishg.rest.controllers;

import com.krrishg.rest.models.BatteryRequest;
import com.krrishg.usecases.CreateBatteriesUseCase;
import com.krrishg.usecases.FindBatteriesUseCase;
import com.krrishg.usecases.models.BatteryInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

class PowerPlantControllerTest extends BaseControllerTest {

    @MockBean
    private CreateBatteriesUseCase createBatteriesUseCase;
    @MockBean
    private FindBatteriesUseCase findBatteriesUseCase;

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
    }

    @Test
    void testSaveBatteries() throws Exception {
        BatteryRequest batteryRequest = new BatteryRequest();
        batteryRequest.setPostcode("111");
        batteryRequest.setCapacity(100);
        batteryRequest.setName("request");
        MockHttpServletResponse response = mvc
                .perform(MockMvcRequestBuilders.post("/batteries")
                        .content(mapToJson(Arrays.asList(batteryRequest)))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andReturn()
                .getResponse();

        assertEquals(201, response.getStatus());
        Mockito.verify(createBatteriesUseCase).execute(any(BatteryInput.class));
    }

    @Test
    void testFindBatteries() throws Exception {
        MockHttpServletResponse response = mvc
                .perform(MockMvcRequestBuilders.get("/batteries/range/1000/3000")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andReturn()
                .getResponse();

        assertEquals(200, response.getStatus());
        Mockito.verify(findBatteriesUseCase).execute("1000", "3000");
    }
}