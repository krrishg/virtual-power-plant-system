package com.krrishg.usecases;

import com.krrishg.repository.BatteryRepository;
import com.krrishg.repository.entities.Battery;
import com.krrishg.usecases.models.BatteryOutput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FindBatteriesUseCaseTest {
    private BatteryRepository batteryRepository;
    private FindBatteriesUseCase findBatteriesUseCase;

    @BeforeEach
    void setup() {
        batteryRepository = Mockito.mock(BatteryRepository.class);
        findBatteriesUseCase = new FindBatteriesUseCase(batteryRepository);
    }

    @Test
    void givenPostCodeRangeOfNonExistingBatteries_whenExecute_thenShouldReturnNull() {
        Mockito.when(batteryRepository.findByPostcodeBetweenOrderByNameAsc("1", "5")).thenReturn(null);
        final String from = "1";
        final String to = "5";
        BatteryOutput output = findBatteriesUseCase.execute(from, to);
        assertNull(output);
    }

    @Test
    void givenRangeOfPostCode_whenExecute_thenShouldProvideCorrectSummary() {
        Battery first = createBattery("first", 100, "1000");
        Battery second = createBattery("second", 200, "2000");
        List<Battery> batteries = Arrays.asList(first, second);

        Mockito.when(batteryRepository.findByPostcodeBetweenOrderByNameAsc("500", "5000")).thenReturn(batteries);
        String from = "500";
        String to = "5000";
        BatteryOutput output = findBatteriesUseCase.execute(from, to);

        assertEquals(300, output.getTotalCapacity());
        assertEquals(150, output.getAverageCapacity());
    }

    private Battery createBattery(String name, Integer capacity, String postCode) {
        Battery battery = new Battery();
        battery.setPostcode(postCode);
        battery.setName(name);
        battery.setCapacity(capacity);
        return battery;
    }
}