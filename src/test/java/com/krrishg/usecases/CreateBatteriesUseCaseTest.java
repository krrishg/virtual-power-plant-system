package com.krrishg.usecases;

import com.krrishg.repository.BatteryRepository;
import com.krrishg.usecases.models.BatteryInput;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

class CreateBatteriesUseCaseTest {

    @Test
    void givenBatteryInput_whenExecute_thenShouldSaveToDatabase() {
        BatteryRepository repository = Mockito.mock(BatteryRepository.class);

        BatteryInput batteryInput = new BatteryInput();
        batteryInput.setBatteries(new ArrayList<>());

        CreateBatteriesUseCase useCase = new CreateBatteriesUseCase(repository);
        useCase.execute(batteryInput);

        Mockito.verify(repository).saveAll(batteryInput.getBatteries());
    }

}